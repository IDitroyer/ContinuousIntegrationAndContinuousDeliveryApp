@echo off
SET /A log=%RANDOM% * 100 / 32768 + 1

SET project=test
SET jarFile=continuousintegrationandcontinuousdeliveryapp-0.0.7-SNAPSHOT.jar
SET jenkinsRoute=c:\jenkins\workspace
SET port=8090

FOR /F "tokens=5 delims= " %%P IN ('netstat -a -n -o ^| findstr 0.0.0.0:%port%') DO (TaskKill.exe /PID %%P /F)

del /s /q %jenkinsRoute%\%project%\deployment\*
xcopy %jenkinsRoute%\%project%\target\%jarFile%* %jenkinsRoute%\%project%\deployment\%jarFile%* /K /D /H /Y

%jenkinsRoute%\%project%\running.bat