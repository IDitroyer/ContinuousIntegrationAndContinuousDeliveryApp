FROM java:8
WORKDIR /
ADD continuousintegrationandcontinuousdeliveryapp-0.0.7-SNAPSHOT.jar continuousintegrationandcontinuousdeliveryapp-0.0.7-SNAPSHOT.jar
EXPOSE 8090
CMD java -jar continuousintegrationandcontinuousdeliveryapp-0.0.7-SNAPSHOT.jar
